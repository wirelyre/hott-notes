## About

This repository contains notes on *Homotopy Type Theory: Univalent Foundations
of Mathematics*. The notes are written in Markdown, with each chapter a
separate file.

The `master` branch contains just the source Markdown, but the `rendered`
branch also contains files `build/….html`, which were built from the source
using Pandoc. You can build them yourself with `make`.

There are tags for each chapter as I read and take notes on them.
