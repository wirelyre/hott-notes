.PHONY: all clean

all: $(patsubst src/%.md,build/%.html,$(wildcard src/*.md))
clean:
	rm -rf build

build/%.html : src/%.md
	@mkdir -p build
	pandoc -s -t html --katex $< -o $@
