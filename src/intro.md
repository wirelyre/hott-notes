% Introduction

- sorry, lots of notes for this introduction, because it keeps reminding me of
  stuff and then moves to another topic

- homotopy theory <=>? type theory

- what do I know about homotopies off the top of my head?

  - definition
    - given a space; given two points; given two continuous paths between those
        points; the paths are homotopic if there exists a continuous function
        between those paths
    - that is, given $x$, $y \in S$; $f$, $g : [0, 1] \to S$ continuous and
        $f(0)=g(0)=x$, $f(1)=g(1)=y$; then $f$ and $g$ homotopic means $H : [0,
        1] \to ([0, 1] \to S)$ continuous and $H(0)=f$ and $H(1)=g$.
    - that is, a homotopy from $x$ to $y \in S$ is $H : [0, 1] \times [0, 1] \to
        S$ continuous where $H(\_, 0) = x$ and $H(\_, 1) = y$
    - in a sense, "homotopy" is an operation on a space to two dimensions higher
    - in another, it's an identification between two things, so just one
        dimension higher

  - group
    - given a fixed point $x$, take an equivalence class of paths
    - two paths are equivalent if they are homotopic
    - then that equivalence class is a group
      - identity is the trivial homotopy
      - group operation $f * g$: take some path in $f$ and some in $g$, then
        link them together
    - this is interesting because some paths are not homotopic
      - e.g. on a torus the identity is different from a path around the hole
      - but on a sphere all paths are homotopic

  - I think there's some fancy definition similarly powerful to the homotopy
    group but more convenient that people use for knot theory?

  - groupoid: basically unfix the point $x$ and use arrows like a category

- anyway, in this book we're interested in identifying two things as equivalent
  using this kind of thing
  - spoiler, homotopies can also be homotopic in a higher space

- Wikipedia says homotopy doesn't need to be a "path" from $[0, 1]$; makes sense

- something about univalence, or in other words turning isomorphism into a
  fundamental idea

- I've read about the foundations of type theory
  - Frege's Begriffsschrift is great
  - > Jean van Heijenoort. From Frege to Godel: A Source Book in Mathematical
      Logic, 1879–1931, Harvard University Press, Cambridge, MA.
    thanks, Wikipedia, I actually read this a few years ago
  - too bad about the inconsistency

- do not identify types with sets, but rather with abstract spaces; allows for
  more subtle kinds of equality

  - I have questions about "continuity" with the type $A = \{a, b\}$. is it
    discrete? what is the concrete topology for $A$ in this context?
  - the next section "Homotopy type theory" does mention not having to develop
    point-set topology; not satisfying to me
  - okay, I understand treating them as groupoids
    - I'm already learning

- uh oh, sheaves, no idea what those are
- we want to make the meta-logic kind of intuitionistic but very expressive

- what, if anything, does a linear logic look like here?

- basic question: how can we treat equivalences between equivalences? even
  within a single universe, things get fragile in the higher orders
  - I'm getting confused by all the levels in this discussion but I'll get back
    to it later
  - in the end I guess we get to lift identity into equivalence
  - "identifying isomorphic structures" reminds me of duality; e.g. dual
    geometric constructions, or dual theorems in category theory
    
- inductively defined structures = well-founded trees
  - so coinductively defined = co-trees? do those link somehow?
- I've seen something like these geometric "inductively defined spaces" before;
  a lecture on projective geometric algebra made a sphere as a rotated circle
  (outer product?) and a torus as a rotated shifted circle
- lots of homotopy jargon

- reference: [F. William Lawvere. An elementary theory of the category of
  sets]
- does this imply that ZF cannot be modeled in HoTT?
  - mentions that objects in ZF can be characterized by inductive universal
    properties

- this book isn't in deductive style, thank goodness

- propositions are types because logical connectives (given ... implies ...) are
  like type constructions (`\_ -> _`) ($\prod_{...} ...$)
  - but propositions as spaces identifies homotopies/equivalences for proofs
    - cool, and the very construction of the proposition $A \land B$ implies
      some structure of equivalences

- Postnikov sections sound important, what are they?

- looks like we can have excluded middle at only some level of the theory, which
  is safe
- contrary to above "kind of intuitionistic", we don't need to assume any
  intuitionism but it's consistent
  
- everything in the book modeled in Kan complexes

- what is the mentioned "normalization" in type theory?
  - I thought I read that dependent type checking is undecidable?
  - no, just inference; via [here], typability is undecidable with unannotated
    terms in a non-empty context

- I know the Dedekind construction of $ℝ$; what is the Cauchy construction?
  something about convergence (metric completion)?

[F. William Lawvere. An elementary theory of the category of sets]: http://www.tac.mta.ca/tac/reprints/articles/11/tr11.pdf
[here]: https://cs.stackexchange.com/questions/12691/what-makes-type-inference-for-dependent-types-undecidable
