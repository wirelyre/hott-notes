% Chapter 1: Type theory

Given the section titles, I expect this to be very smooth until 1.12: Identity
types. The rest seems like totally standard and comfortable type theory that I
know from e.g. Coq and Lean.

In the past I have found the identity proposition $a = a$ very confusing --- it
looks like it has two places, but there must be only one constructor, right?
What kind of thing is it? Is it only for judgmental equality?

Let's find out.

## Type theory versus set theory

- in set-theoretic foundation, logic as deductive system, then also axioms

- here, type theory *is* a deductive system, with propositions as types

  - similarly, the judgement "$A$ has a proof" becomes the judgement "$a : A$"

- set membership is a question (is 1.2 an integer?), but typing is part of the
  very identity of the thing we name (let $a : A$) or else a property given
  things already named and typed ($f(x) : B$)

- also have judgmental equality $a \equiv b : A$, again never a question but
  part of the very identity of the things we name/make

- $f : A \to B$ standard mathematical notation, but also a typing judgement
  since $A \to B$ can represent a type

- judgements depend on context of named things; names are "assumptions"

  - an assumption $h : H$ where $H$ a proposition is a "hypothesis". let's take
    that apart:
    - $H$ is a proposition, something that maybe could be true
    - $h$ is a proof that $H$ is true, maybe conditional on other things we know
    - in whatever thing we're discussing given this context, we can *assume* $h$
    - so $h$ is a hypothesis

  - we *can* "assume" propositional equality $x = y$ because it's provable
    - we *cannot* "assume" judgmental equality $x \equiv y$ because it's not
      even a question
    - we *can* substitute $a$ for $x$ in an expression $P(x)$ given $a \equiv x$

- rules are type formers, ways to construct types and behaviors of elements
  (like reductions in lambda calculus)

## Function types

- equivalent:
  - $f : A \to B$, $f(x) :\equiv \Phi$
  - $f \equiv (\lambda(x:A). \Phi) : A \to B$
  - $f \equiv (\lambda x. \Phi) : A \to B$ because $x : A$ easily inferable

- $\beta$-reduction / application as usual
- $\eta$-expansion / function name substitution as usual

- currying

## Universes and families

- universe hierarchy $\mathcal{U}_0 : \mathcal{U}_1 : \mathcal{U}_2 : \cdots$

  - also if $A : \mathcal{U}_i$, then $A : \mathcal{U}_{i+1}$
    - unfortunately elements don't have unique types
    - (take this further with [Cedille], see Stump, The Calculus of Dependent
      Lambda Eliminations)

  - how can you prove False from $\mathcal{U}_\infty : \mathcal{U}_\infty$?

  - if in some concrete universe $i$, types belonging to $\mathcal{U}_i$ are
    "small types"

- families of types are functions to a universe (given a something (type
  parameter), produce a type)

  - e.g. constant type family at type $B : \mathcal{U}$, namely $(\lambda(x :
    A). B) : A \to \mathcal{U}$

  - note that universe indices are not natural numbers definable in this theory
    - if they were, then what type for $\lambda(i:\mathbb{N}). \mathcal{U}_i$?
    - would have to be $\mathbb{N} \to \mathcal{U}_\infty$, oops
      - actually, could be $\prod_{i : \mathbb{N}} F(i)$; probably a reason to
        disallow that

[Cedille]: https://cedille.github.io/

## Dependent function types ($\Pi$-types)

- strictly more general than arrow functions

- this notation is always a little uncomfortable to me, so let's play with it:

  - $f : A \to B$
  - $f : \prod_{\_ : A} B$
  - $f : \prod_{x : A} B$
  - $f : \prod_{x : A} (\operatorname{id} B\space{}x)$
  - $f : \prod_{x : A} (\lambda(\_:A). B)(x)$

- so we can just change the expression $\operatorname{id} B$ to some type family
  depending on its parameter $x : A$; then an element $f$ of that type is a
  dependent function

- (we can write chains of types like $A : B : C$ where $B$ and $C$ are
  universes)
  - does every term have a type?

- I've seen capital $\Lambda$ abstractions before --- what are those?

- example of $\mathrm{fmax}$ is a little weird
  - each type in the family $\mathrm{Fin} : \mathbb{N} \to \mathcal{U}$ is
    basically a refinement of $\mathbb{N}$, where specifically
    $\operatorname{Fin} n \equiv \{x : \mathbb{N}, x < n\}$
  - so $\operatorname{Fin} 0 \subset \operatorname{Fin} 1 \subset \cdots \subset
    \mathbb{N}$
  - then in one sense (under covariance) $\mathrm{fmax} : \mathbb{N} \to
    \mathbb{N}$
  - but actually it's more specific than that, because $\operatorname{fmax} n :
    \operatorname{Fin} (n+1)$
  - so what is the type of $\mathrm{fmax}$? well, it depends on the argument, so
    $\prod_{n : \mathbb{N}} \Box$
  - thus $\mathrm{fmax} : \prod_{n : \mathbb{N}} \operatorname{Fin} (n+1)$ where
    $\operatorname{fmax} n = n$
  - we can check that indeed $n : \operatorname{Fin} (n+1)$ because $n < n+1$

- now we can have applications in type position too, $f(a) : B(a)$

- $\mathrm{id}$ is a bit crazy now, since it's polymorphic over some universe
  - $\operatorname{id} \equiv \prod_{A : \mathcal{U}} \prod_{x : A} A$
  - but since we don't use $x$, that's the same as $\prod_{A : \mathcal{U}} A
    \to A$

- can we be polymorphic over universes?

## Product types

- unit $\mathbf{1} : \mathcal{U}$, which is the unit (identity) of type products

  - hmm, up to isomorphism?
  - $\mathbf{*} : \mathbf{1}$ unique

- when we make *a new kind of type*:
  - how to make types of this kind
  - how to make elements of those types
  - how to eliminate/use elements (?)
    - I guess this is type-level rules
    - e.g., when defining $\Box \to \Box$; given $f : A \to B$ and $x : A$, then
      $f(x) : B$
  - how to compute with eliminators
  - maybe a uniqueness principle
    - what is this? maybe clearer by &sect;2.15
    - uniqueness and computation can sometimes be propositional, not judgmental

- so for products:
  - given $A$, $B : \mathcal{U}$, then $A \times B : \mathcal{U}$
  - given $a : A$, $b : B$, then $(a, b) : A \times B$
  - given $(a, b) : A \times B$, $g : A \to B \to C$, then form $f : A \times B
    \to C$ where $f((a, b)) :\equiv g(a)(b)$
  - what are adjoint functors and how are products and exponentials adjoint?
    - I have some intuition for this (we converted $A \times B \to C$ to $A \to
      B \to C$ above) but haven't held adjunction before; just need to touch the
      hom-sets

- instead of working in metatheory to make $f$ from $g$ above, let
  $$ \mathrm{rec}_{A \times B} : \prod_{C : \mathcal{U}} (A \to B \to C) \to A
     \times B \to C $$
  $$ \mathrm{rec}_{A \times B}(C, g, (a, b)) :\equiv g(a)(b) $$
  - a "recursor" since with actual inductive types we'll recurse for each
    constructor

- trivially $\mathrm{rec}_\mathbf{1} : \prod_{C : \mathcal{U}} C \to \mathbf{1}
  \to C$ where $\mathrm{rec}_\mathbf{1}(C, c, \mathbf{*}) = c$
  - isomorphic to $\mathrm{id}$?

- the recursor isn't general enough --- remember that $\Box \to \Box$ is a
  special case of $\prod$
  - how do we generalize $A \to B \to C$? let $C$ depend on values $a : A$ and
    $b : B$
  - in other words, $C$ is a family $C : A \to B \to \mathcal{U}$
  - then we have an eliminator
    $$ \operatorname{elim}_{A \times B} : \prod_{C : A \to B \to \mathcal{U}}
       \left(\prod_{x : A} \prod_{y : B} C(x)(y)\right) \to
       \prod_{x : A \times B} C(x) $$
    $$ \operatorname{elim}_{A \times B}(C, g, (a, b)) :\equiv g(a)(b) $$
  - actually we have one more piece of information, namely that the pair $(a,
    b) : A \times B$ has already been formed
  - so we can write the family $C : A \times B \to \mathcal{U}$, finally giving
    the provided
    $$ \operatorname{ind}_{A \times B} : \prod_{C : A \times B \to \mathcal{U}}
       \left(\prod_{x : A} \prod_{y : B} C((x, y))\right) \to
       \prod_{x : A \times B} C(x) $$
    $$ \operatorname{ind}_{A \times B}(C, g, (a, b)) :\equiv g(a)(b) $$
  - are the $C : A \to B \to C$ and $C : A \times B \to C$ equivalent? do they
    both work in general for any inductive construction?

- induction on unit:
  $$ \operatorname{ind}_\mathbf{1} : \prod_{C : \mathbf{1} \to \mathcal{U}}
     C(\mathbf{*}) \to \prod_{x : \mathbf{1}} C(x) $$
  $$ \operatorname{ind}_\mathbf{1}(C, c, \mathbf{*}) :\equiv c $$
  - basically generalizes "a family / predicate given some unit" to "that family
    / predicate on all units"

## Dependent pair types ($\Sigma$-types)

- the type of the second component of pairs can depend on the first component

  $$ \sum_{x : A} B(x) : \mathcal{C} $$
  $$ \sum_{x : A} B \equiv A \times B $$

- when reading $\Pi$ and $\Sigma$ types, the big operator actually stands for "a
  parameter of some sort, *right now*"
  - I tend to read it inside out, like I read $\sum_{n=1}^{10} n^2$; I read the
    inside expression, understand it, then figure out what the free variable
    does
  - but that's wrong! $\sum_{a : A} B(a)$ means *I provide an $A$* (then
    something)
  - and $\prod_{a : A} B(a)$ means *you provide an $A$* (then something)
  - the order matters; the subscript conceptually happens "before" the inner
    expression

- in any case I see the draw of notation like $\sum(x : A), B(x)$ --- it looks
  like a pair
  - actually it looks kind of like a lambda binding, as $\lambda(x :
    \mathbb{N}), x+x$

- first projection (component) of $\Sigma$-type easy to type,
  $$ \operatorname{pr}_1 : \left(\sum_{x : A} B(x)\right) \to A $$

- type of second projection depends on *value* of first projection
  $$ \operatorname{pr}_2 : \prod_{p : \sum_{x : A} B(x)}
     B(\operatorname{pr}_1(p)) $$

- for the recursor for $\sum_{x : A} B(x)$:
  - given any type $C : \mathcal{U}$
  - and a function of two parameters, the first $x : A$ and the second $\_ :
    B(x)$, creating a $C$
  - then we have a function transforming the pair type to a $C$
  $$ \operatorname{rec}_{\sum_{x : A} B(x)} : \prod_{C : \mathcal{U}}
     \left( \prod_{x : A} B(x) \to C \right) \to
     \left( \sum_{x : A} B(x) \right) \to C $$
  - very subtle, basically turning a function of two parameters, typed by $\Pi$,
    into a function of a single parameter, that single parameter typed by
    $\Sigma$

- the induction operator is a monster:
  - given any type family $C$, parameterized by a dependent pair (a value)
  - transform a function of two parameters, which returns a value of the correct
    type in the family $C$
  - into a function of a single parameter, a dependent pair
  - returning a value of the correct type in the family $C$
  $$ \operatorname{ind}_{\sum_{x : A} B(x)} :
     \prod_{C : \left( \sum_{x : A} B(x) \right) \to \mathcal{U}}
     \left( \prod_{a : A} \prod_{b : B(a)} C((a, b)) \right) \to
     \prod_{p : \sum_{x : A} B(x)}    C(p) $$

- "type-theoretic axiom of choice", which is almost a tautology due to the way
  $\Pi$- and $\Sigma$-types are constructed

- useful e.g. for "type classes" with signatures (algebras)
  - e.g. "a semigroup is a type and a binary operation such that the operation
    is associative"
    $$ \mathrm{semigroup} :\equiv \sum_{A : \mathcal{U}}
       \sum_{f : A \to A \to A}
       \prod_{x : A} \prod_{y : A} \prod_{z : A}
       f(f(x, y), z) = f(x, f(y, z)) $$
  - (is it confusing to write $\prod_{x, y, z : A}$?)

## Coproduct types

- given $A$, $B : \mathcal{U}$, then $A + B : \mathcal{U}$

  - left and right injections as usual
  - the zero is $\mathbf{0} : \mathcal{U}$, with no constructors

- $$ \operatorname{rec}_{A + B} : \prod_{C : \mathcal{U}}
     (A \to C) \to (B \to C) \to A + B \to C $$
- $$ \operatorname{ind}_{A + B} : \prod_{C : (A + B) \to \mathcal{U}}
     \left( \prod_{a : A} C(\operatorname{inl}(a)) \right) \to
     \left( \prod_{b : B} C(\operatorname{inl}(b)) \right) \to
     \prod_{x : A + B} C(x) $$
  - I think this is clearer than the general induction operator for dependent
    pairs above

- by the way, $\operatorname{rec}_\mathbf{0} : \prod_{C : \mathcal{U}}
  \mathbf{0} \to C$

- by analogy to dependent products, is there a dependent co-product?

- why do we write $\Pi$ for exponential types and $\Sigma$ for product types?
  conventionally $\Sigma$ is for a sum (like coproducts) and $\Pi$ is for
  products

## The type of booleans

- let $\mathbf{2} : \mathcal{U}$ where $0_\mathbf{2}$, $1_\mathbf{2} :
  \mathbf{2}$ and no others

- recursor and induction are essentially if-then-else

- could have defined $A + B :\equiv \sum_{x : \mathbf{2}}
  \operatorname{rec}_\mathbf{2}(\mathcal{U}, A, B, x)$

  - a value of which is either $0$ or $1$, and based on that, either an $A$ or a
    $B$
  - very subtle

- could have defined $A \times B :\equiv \prod_{}

  - a value of which is a function taking either $0$ or $1$, and based on that,
    either an $A$ or a $B$

## The natural numbers

- an inductive type with two constructors,
  $$ \begin{aligned}
     0 &: \mathbb{N} \\
     \mathrm{succ} &: \mathbb{N} \to \mathbb{N}
     \end{aligned} $$

- can think of inductive types as being defined by their eliminators, from which
  primitive recursion arises naturally

  $$ \begin{aligned}
     \operatorname{rec}_\mathbb{N} &:
        \prod_{C : \mathcal{U}} C \to (\mathbb{N} \to C \to C)
        \to \mathbb{N} \to C \\
     \operatorname{ind}_\mathbb{N} &:
        \prod_{C : \mathbb{N} \to \mathcal{U}} C(0) \to
        \left(\prod_{n : \mathbb{N}} C(n) \to C(\operatorname{succ}(n))\right)
        \to \prod_{n : \mathbb{N}} C(n)
     \end{aligned} $$

 - cool definition of $\mathrm{add}$, let's figure it out: $\mathrm{add} :\equiv
   \operatorname{rec}_\mathbb{N} (\mathbb{N} \to \mathbb{N}, \lambda{}n. n,
   \lambda{}n\space{}g\space{}m. \operatorname{succ}(g(m)))$
   - $\operatorname{add} n : \mathbb{N} \to \mathbb{N}$ adds $n$ to its input
   - in the zero case, $\operatorname{add} 0$ is a function returning its input,
     $\lambda{}n.n$
   - in the successor case, given a left summand $n$, and an $n-1$-adder $g$,
     return a new function which does the $n-1$ adding, plus one,
     $\lambda{}n\space{}g\space{}m. \operatorname{succ}(g(m))$

- in a sense, induction is about removing constructors from function types
  - opposite view of "breaking elements down by constructor"
  - given types for specific values of this inductive type,
  - present a type for any value

- something important about the nature of $=$ identity here, but I'll be hecked
  if I can tease it apart

## Pattern matching and recursion

- pattern matching is shorthand for induction
- recursive function calls should be thought of as "that which has already been
  computed for a structurally smaller value"

## Propositions as types

- not "this proposition is true", but "I have a witness (proof) of this
  proposition"

- here, proofs are mathematical objects

- what do dependent types mean as propositions?
  - $\sum_{a : A} B(a)$: "I have an $a : A$ (e.g. an integer)" and furthermore I
    know some property about it $B(a)$
    - also maybe: I know $A$, and furthermore, given how I know it, I know a
      specific property of it $B$
  - $\prod_{a : A} B(a)$: given any $a : A$ (e.g. an integer), I know some
    property about it $B(a)$

- $\neg A :\equiv A \to \mathbf{0}$, or "if $A$ then a contradiction"

- I think I read that under constructive logic, $\neg \neg \neg A \to \neg A$
  holds. is that true?

- I'm curious to express $P : (A \to \mathbf{0}) \times (B \to \mathbf{0}) \to
  (A + B \to \mathbf{0})$ in terms of $\operatorname{rec}_{\Box \times \Box}$
  and $\operatorname{rec}_{\Box + \Box}$ but I don't want to try it without a
  type checker

- is there an accessible reason that $\neg(A \land B) \to (\neg A \lor \neg B)$
  doesn't hold? something about the negated assumption being impossible to break
  apart?

- hey, the semigroup definition I expressed above

- "if and only if", in the sense of $(A \to B) \times (B \to A)$, is for types
  regarded as propositions and doesn't necessarily mean anything important about
  the structure of either $A$ or $B$ regarded as other kinds of types

- knowing that "$A$ inhabited" means we demonstrate $a : A$; "uninhabited" means
  we demonstrate $na : \neg A$

- does it make sense to wonder "are all types inhabited or uninhabited?"

## Identity types

- this makes much more sense now

  - $\operatorname{Id}_A(a : A, b : A)$ is a *proposition* (a type) that $a = b$
  - I can write the name of this type for any $a, b : A$, watch: $3 =_\mathbb{N}
    4$
  - but when can I demonstrate that it is inhabited?
  - at the very least, we *should* have it inhabited for two judgmentally equal
    values, so we can demonstrate it inhabited by a constructor $\mathrm{refl}$
    $$ \mathrm{refl} : \prod_{a : A} (a =_A a) $$
  - now *I get to decide* what other kinds of equality there should be, and what
    it implies (how it is eliminated)
    - (if we can't eliminate equality in some way then it's a black hole of
      information)
  - so we'd better make this match *exactly* with a naive interpretation

- for each $C : A \to \mathcal{U}$, there exists $f : \prod_{x, y : A}
  \prod_{p : x =_A y} C(x) \to C(y)$ such that $f(x, x, \operatorname{refl}_x)
  :\equiv \operatorname{id}_{C(x)}$
  - why write the conclusion with judgmental equivalence? if the whole thing
    were written as a proposition we could conclude with propositional equality,
    right?
  - actually, why that conclusion at all? what else does that get us?
  - in any case this is quite clean and beautiful

- full induction is key because we can care about the reason (witness) that $a =
  b$, not just that it does

  $$ \operatorname{ind}_{=_A} :
     \prod_{C : \prod_{x, y : A} (x =_A y) \to \mathcal{U}}
     \left( \prod_{x : A} C(x, x, \operatorname{refl}_x) \right) \to
     \prod_{x, y: A} \prod_{p : x =_A y} C(x, y, p) $$
  $$ \operatorname{ind}_{=_A} (C, c, x, x, \operatorname{refl}_x)
     :\equiv c(x) $$

  - given any property $C$ about $x$ and $y$ and *why* $x = y$ if they are equal
  - and knowing that $C$ holds for judgmentally equal $x$ and $x$
  - then $C$ holds for any $x$ and $y$ when $x = y$
  - called "path induction" for reasons we haven't mentioned yet (I guess
    because with the rest of our definitions we haven't made yet, it works like
    homotopies)

- in other words, for *any binary property* $C$, as long as $C(x, x)$ holds,
  then for any $x = y$ also $C(x, y)$ holds
  - but slightly more, the property $C$ can depend on the nature of the equality
    $x = y$
  - very powerful

- since the binder for $C$ is outside the binders with $A$, the property $C$ has
  to be expressed as "forall"s on $x$ and $y$
  - we can instead move one binder with $A$ first, so $C$ can be expressed in
    one unknown ("forall") $x : A$ and one (arbitrary) fixed $a : A$
  - "based" path induction because we have a "base" $a : A$

  $$ \operatorname{ind'}_{=_A} :
     \prod_{a : A}
     \prod_{C : \prod_{x : A} (a =_A x) \to \mathcal{U}}
     C(a, \operatorname{refl}_a) \to
     \prod_{x : A} \prod_{p : a =_A x} C(x, p) $$
  $$ \operatorname{ind'}_{=_A}(a, C, c, a, \operatorname{refl}_a) :\equiv c $$

- if I'm reading this right:
  - although there may be a bunch of different forms of identity
  - if we care about a property of a "forall $x = y$"
  - we need only consider the form $\operatorname{refl}$
  - because, using a homotopy interpretation, the "forall" quantifier on $y$
    makes any identity trivial
  - because if $y$ is allowed to vary, then given a path from $x$ to $y$, $y$
    could be anywhere on that path
  - we can just continuously move $y$ backwards on the path until we have the
    trivial loop at $x$
  - so $\_ : x = y$ and $\operatorname{refl} x : x = x$ are themselves identical
    (homotopic)

- from these we have that equality is an equivalence relation (reflexive,
  symmetric, and transitive)

- disequality $(x \ne_A y) :\equiv \neg(x =_A y)$

- this is an intentional type theory, where judgmental equality is different
  from our propositional equality
  - in this specific one, proofs of equality are relevant
  - and judgmental equality includes &eta;-reduction

- what is "deep pattern matching"? (ref. Coq92b)

- towards &sect;3.2&ndash;3.3, why not include a primitive type of propositions?

- why not $B : A \to \mathcal{U}_i$ and $i \le j$ imply $B : A \to
  \mathcal{U}_j$? is there an axiom schema that makes that all just work?
